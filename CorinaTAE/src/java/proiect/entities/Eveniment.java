/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiect.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CORI
 */
@Entity
@Table(name = "EVENIMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eveniment.findAll", query = "SELECT e FROM Eveniment e")
    , @NamedQuery(name = "Eveniment.findById", query = "SELECT e FROM Eveniment e WHERE e.id = :id")
    , @NamedQuery(name = "Eveniment.findByNume", query = "SELECT e FROM Eveniment e WHERE e.nume = :nume")
    , @NamedQuery(name = "Eveniment.findByDescriere", query = "SELECT e FROM Eveniment e WHERE e.descriere = :descriere")
    , @NamedQuery(name = "Eveniment.findByNrParticipanti", query = "SELECT e FROM Eveniment e WHERE e.nrParticipanti = :nrParticipanti")})
public class Eveniment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 250)
    @Column(name = "NUME")
    private String nume;
    @Size(max = 2000)
    @Column(name = "DESCRIERE")
    private String descriere;
    @Column(name = "NR_PARTICIPANTI")
    private Long nrParticipanti;

    public Eveniment() {
    }

    public Eveniment(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public Long getNrParticipanti() {
        return nrParticipanti;
    }

    public void setNrParticipanti(Long nrParticipanti) {
        this.nrParticipanti = nrParticipanti;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eveniment)) {
            return false;
        }
        Eveniment other = (Eveniment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "proiect.entities.Eveniment[ id=" + id + " ]";
    }
    
}
