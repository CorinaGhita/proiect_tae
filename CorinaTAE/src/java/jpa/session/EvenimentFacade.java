/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import proiect.entities.Eveniment;

/**
 *
 * @author CORI
 */
@Stateless
public class EvenimentFacade extends AbstractFacade<Eveniment> {

    @PersistenceContext(unitName = "CorinaTAEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EvenimentFacade() {
        super(Eveniment.class);
    }
    
}
